jQuery(document).ready(function($){

function keydownOpen(element, code, parentChildren, parentClass, _class, removeTabIndex){
    $(element).bind('keydown', function(e, l){


        var keycode = (e.keycode ? e.keyCode : e.which);
        if(keycode === code){
            e.preventDefault();
            $(this).parent('li').toggleClass(parentClass);
            $(this).siblings(parentChildren).toggleClass(_class);
            $(this).siblings(parentChildren).find('li:first-child > a')[0].focus();

            if($(this).siblings(parentChildren).attr('tabindex') === '1'){
                $(this).siblings(parentChildren).attr('tabindex', '-1');
                $(this).siblings(parentChildren).find('*').attr('tabindex', '-1');
            }else{
                $(this).siblings(parentChildren).attr('tabindex', '1');

                if(removeTabIndex === true){
                    $(this).siblings(parentChildren).find('*').removeAttr('tabindex');
                }
            }

            // $(this).unbind(e.preventDefault());

        }
    });
}

function keydownClose(element, code, parent, _class){
    $(element).bind('keydown', function(e){
        var keycode = (e.keycode ? e.keyCode : e.which);

        if(keycode === code){
            $(this).closest(parent).children('a').focus();
        }
    });
}

keydownOpen('.accountLinks__menu-hover > a', 13, '#accountMenu','',  'open', true);
keydownClose('#accountMenu', 13, '.accountLinks__menu-hover');

keydownOpen('.mainNav__menu-link', 13, '.mainNav__submenu', '', 'open', true);
keydownClose('.mainNav__submenu', 13, '.mainNav__menu-item');

keydownOpen('.js-popoutMenu_toggler', 13, 'ul', 'popoutNav__popout-submenu--is-open', 'open', false);

keydownOpen('.cartLink > a', 13, '#minicart','',  'open', true);
keydownClose('#minicart > *', 13, '.cartLink');

$('.mainNav__menu > li > a').on('focus mousenter', function(e){
    var subMenu = $('.mainNav__submenu');

    if(subMenu.hasClass('open')){
        subMenu.removeClass('open');
    }
});


function closePopout(){
    $('.page').removeClass('popoutNav__popout--is-open');
    menuItems.removeClass('popoutNav__popout-submenu--is-open');
    menuItems.find('ul').slideUp('fast');
    // menuItems.not(':first-child').removeClass('popoutNav__popout-submenu--is-open');
    // menuItems.not(':first-child').find('ul').slideUp('fast');
    // $( '.popoutNav__popout-menu-item:first-child' ).addClass('popoutNav__popout-submenu--is-open').find('.popoutNav__popout-submenu').slideDown('fast');
    // submenuItems.removeClass('popoutNav__popout-subsubmenu--is-open').find('ul').slideUp('fast');
    $('.js-popoutNav_trigger').focus();
    $('*').removeAttr('tabindex');
}



if($('.js-popoutNav_trigger')[0]){
    var popoutNav         = $('.popoutNav__popout');
    var popoutNavChildren = popoutNav.find( 'a, button');



    $( '.js-popoutNav_trigger' ).on( 'click', function() {

        $('.popoutNav__popout').show();
        $('.popoutNav__popout-menu').find('li:first-child').focus();
        $('.popoutNav__hidden').focus();

        popoutNavChildren.each(function(){
            $(this).attr('tabindex', 0);
        });

        setTimeout(function(){
            $( '.page' ).addClass('popoutNav__popout--is-open');
            popoutNav.attr('tabindex', 0);
            // $('*').not('.popoutNav__popout, .popoutNav__popout *').attr('tabindex', -1);
            // $('.productCarousel').children('*').attr('tabindex', -1);
        }, 100);
    });
}


var popoutMenuToggler = $( '.js-popoutMenu_toggler' );

if(popoutMenuToggler[0]){
    var menuItems         = $( '.popoutNav__popout-menu-item' );
    var submenuItems      = $( '.popoutNav__popout-submenu-item' );

    popoutMenuToggler.click(function(e){ e.preventDefault(); });
    popoutMenuToggler.on('mousedown', function(e){
        e.preventDefault();

        var ariaState = $(this).attr('aria-expanded');
        var parent    = $(this).parent();
        var sibling   = $(this).next('ul');

        if( sibling.hasClass('popoutNav__popout-submenu') ){
            menuItems.not(parent)
                .removeClass('popoutNav__popout-submenu--is-open')
                .find('.js-popoutMenu_toggler').attr('aria-expanded', false);
            submenuItems
                .removeClass('popoutNav__popout-subsubmenu--is-open')
                .find('.js-popoutMenu_toggler').attr('aria-expanded', false);
            parent.toggleClass('popoutNav__popout-submenu--is-open');
        }else{
            submenuItems.not(parent)
            .removeClass('popoutNav__popout-subsubmenu--is-open')
            .find('.js-popoutMenu_toggler').attr('aria-expanded', false);
            parent.toggleClass('popoutNav__popout-subsubmenu--is-open');
        }

        $(this).closest('ul').find('ul').not(sibling).slideUp('fast');

        if( ariaState == "true" ){
            $(this).attr('aria-expanded', "false");
        }else{
            $(this).attr('aria-expanded', "true");
        }
        sibling.slideToggle('fast');
    });
}

var popoutCloser = $( '.js-popoutNav_closer' );

if(popoutCloser[0]){
    popoutCloser.click(function(){
        $('.popoutNav__popout').hide();

        setTimeout(function(){
            closePopout();
        }, 100);

    });
}

// $(window).resize(function(){
//     $('.popoutNav__popout').hide();
//
//     setTimeout(function(){
//         closePopout();
//     }, 100);
// });


document.onkeydown = function(e){
    if(e.keyCode === 27){
        closePopout();
    }
};


if($('.accountLinks__menu-hover, .cartLink__link, .mainNav__dump-hover, .mainNav__submenu-hover')[0]){
    $('.accountLinks__menu-hover, .cartLink__link, .mainNav__dump-hover, .mainNav__submenu-hover').on('mouseenter', function(){
        $(this).attr('aria-expanded', 'true');
    });
}

if($('.accountLinks__menu-hover, .cartLink__link, .mainNav__dump-hover, .mainNav__submenu-hover')[0]){
    $('.accountLinks__menu-hover, .cartLink__link, .mainNav__dump-hover, .mainNav__submenu-hover').on('mouseleave', function(){
        $(this).attr('aria-expanded', 'false');
    });
}






if($('.unbxd_q')[0]){
    $('.unbxd_q').on('keydown', function(){
        $(this).addClass('has-results');
    }).on('focusout', function(){
        $(this).removeClass('has-results');
    });
}


var mainSubHover = $('.js-main-submenu_hover');

if(mainSubHover[0]){
    mainSubHover.mouseenter(function(){
        var thisSubmenu = $(this).find('.mainNav__submenu');
        var thoseSubmenus = $('.mainNav__submenu').not(thisSubmenu);

    thisSubmenu.stop(true, true).slideDown(500);
    thoseSubmenus.stop(true, true).slideUp(500);
});

mainSubHover.mouseleave(function(){
    $(this).find('.mainNav__submenu').stop(true, true).slideUp(500);
});

    mainSubHover.mouseleave(function(){
        $(this).find('.mainNav__submenu').stop(true, true).slideUp('fast');
    });

}

if($('.js-specials_toggler')[0]){
    $('.js-specials_toggler').click(function(){
        $(this).toggleClass('toggled');

        $('.couponBar__specials').slideToggle('fast');
    });
}

$(window).on('resize', function(){
    if( $(this).width() >= 767 ){
        $('.couponBar__specials[style*="none"]').removeAttr('style');
    }else{
        $('.couponBar__specials[style*="block"]').slideUp();
    }

    $('.couponBar__specials-toggler.toggled').removeClass('toggled');
});


var heading = $('.footerLinks__column-heading');

if(heading[0]){

    heading.click(function(){
        if($(window).width() <= 1028){
            var footerMenu = $(this).siblings('.footerLinks__menu');

            footerMenu.slideToggle();
            $(this).toggleClass('footerLinks__column-menu--is-open');
            $('.footerLinks__menu').not(footerMenu).slideUp();
            $('.footerLinks__column-heading').not($(this)).removeClass('footerLinks__column-menu--is-open');
        }
    });
}


if($('.footerSplit__scrollTop')[0]){
    var scrollTop = $('.footerSplit__scrollTop');

    scrollTop.click(function(e){
        e.preventDefault();
        $('html, body').animate({scrollTop: 0}, 500);
        $('html > *, body > *').blur();
    });
}

var popoutNavFocus = $('.popoutNav__hidden');

popoutNavFocus.focus(function(){
    var $firstElement = $('.popoutNav__popout-phone');

    $(this).blur();
    $firstElement.focus();
});

});































































